<?php

require 'db.php';

if (isset($_POST['submit'])) {
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $description = $_POST['description'];
    $title = $_POST['title'];

    db_addBusinesstrip($description, $title);
    header('Location: businesstrips.php');
}

?>

<form action="addbusinesstrip.php" method="post">
    <label for="description">Business Trip Description</label>
    <input type="text" name="description" id="description" required>

    <label for="title">Business Trip Title</label>
    <input type="text" name="title" id="title" required>

    <input type="submit" name="submit" value="Add Business Trip">
</form>