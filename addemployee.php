<?php

require 'db.php';


if (isset($_POST['submit'])) {
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $name = $_POST['name'];
    $title = $_POST['title'];

    db_addEmployee($name, $title);
    header('Location: index.php');
}


?>

<form action="addemployee.php" method="post">
    <label for="name">Employee Name</label>
    <input type="text" name="name" id="name" required>

    <label for="title">Employee Title</label>
    <input type="text" name="title" id="title" required>

    <input type="submit" name="submit" value="Add Employee">
</form>
