<?php

require 'db.php';

if (isset($_POST['submit'])) {
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $businesstrip = $_POST['businesstripid'];
    $employee = $_POST['employeeid'];

    db_addEmployeeToBusinesstrip($businesstrip, $employee);
    header('Location: businesstrips.php');
}

$businesstrips = db_getBusinesstrips();
$employees = db_getEmployees();

?>

<form action="addemployeetotrip.php" method="post">
    <select name="businesstripid" id="businesstripid" required>
        <?php foreach ($businesstrips as $businesstrip) : ?>
            <option value="<?= $businesstrip['businesstripID'] ?>"><?= $businesstrip['title'] ?></option>
        <?php endforeach; ?>
    </select>
    <select name="employeeid" id="employeeid" required>
        <?php $employees = db_getEmployees(); foreach ($employees as $employee) : ?>
            <option value="<?= $employee['employeeID'] ?>"><?= $employee['employee_name'] . ' - ' . $employee['title'] ?></option>
        <?php endforeach; ?>
    </select>

    <input type="submit" name="submit" value="Add Business Trip">
</form>