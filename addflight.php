<?php

require 'db.php';


if (isset($_POST['submit'])) {
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $departure_from = $_POST['departure_from'];
    $destination = $_POST['destination'];
    $employee_ID = $_POST['employee_ID'];
    $number = $_POST['number'];

    db_addFlight($number, $departure_from, $destination, $employee_ID);
    header('Location: index.php');
}


?>

<form action="addflight.php" method="post">
    <label for="number">Flight Number</label>
    <input type="text" name="number" id="number" required>

    <label for="departure_from">Flight Departure From</label>
    <input type="text" name="departure_from" id="departure_from" required>

    <label for="destination">Flight Destination</label>
    <input type="text" name="destination" id="destination" required>

    <label for="employee_ID">Flight Employee</label>
    <select name="employee_ID" id="employee_ID" required>
        <?php $employees = db_getEmployees(); foreach ($employees as $employee) : ?>
            <option value="<?= $employee['employeeID'] ?>"><?= $employee['employee_name'] . ' - ' . $employee['title'] ?></option>
        <?php endforeach; ?>
    </select>

    <input type="submit" name="submit" value="Add Flight">
</form>
