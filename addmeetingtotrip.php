<?php

require 'db.php';

if (isset($_POST['submit'])) {
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $businesstrip = $_POST['businesstripid'];
    $title = $_POST['title'];
    $description = $_POST['description'];

    db_addMeetingToBusinessTrip($businesstrip, $title, $description);
    header('Location: businesstrips.php');
}

$businesstrips = db_getBusinesstrips();

?>

<form action="addmeetingtotrip.php" method="post">
    <select name="businesstripid" id="businesstripid" required>
        <?php foreach ($businesstrips as $businesstrip) : ?>
            <option value="<?= $businesstrip['businesstripID'] ?>"><?= $businesstrip['title'] ?></option>
        <?php endforeach; ?>
    </select>
    <label for="title">Meeting Title</label>
    <input type="text" name="title" id="title" required>

    <label for="description">Meeting Description</label>
    <input type="text" name="description" id="description" required>

    <input type="submit" name="submit" value="Add Meeting Trip">
</form>