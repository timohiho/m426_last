<?php

require 'db.php';

// Get all Business Trips
$businessTrips = db_getBusinessTrips();

// Show Business Trips
echo '<h1>Business Trips</h1>';
echo '<table>';
echo '<tr>';
echo '<th>Business Trip ID</th>';
echo '<th>Business Description</th>';
echo '<th>Business Trip Employees</th>';
echo '<th>Business Trip Title</th>';
echo '<th>Business Trip Meetings</th>';
echo '</tr>';




foreach ($businessTrips as $businessTrip) {

    $trip_employee = db_getBusinessTripEmployees($businessTrip['businesstripID']);
    echo '<tr>';
    echo '<td>' . $businessTrip['businesstripID'] . '</td>';
    echo '<td>' . $businessTrip['description'] . '</td>';
    echo '<td>';
    foreach ($trip_employee as $employee) {
        echo db_getEmployee($employee['employee_ID'])['employee_name'] . ' | ';
    }
    echo '</td>';
    echo '<td>' . $businessTrip['title'] . '</td>';
    echo '<td>';
    $trip_meeting = db_getBusinessTripMeetings($businessTrip['businesstripID']);
    foreach ($trip_meeting as $meeting) {
        echo $meeting['title'] . ' | ';
    }
    echo '</td>';
    echo '</tr>';
}
echo '</table>';
?>

<a href="index.php">Flights</a><br>
<a href="addbusinesstrip.php">Add Business Trip</a><br>
<a href="addemployeetotrip.php">Add Employee to Trip</a><br>
<a href="addmeetingtotrip.php">Add Meeting to Trip</a><br>
