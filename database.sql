create table businesstrip
(
    businesstripID int auto_increment
        primary key,
    description    varchar(45) not null,
    title          varchar(45) not null,
    constraint businesstrip_businesstripID_uindex
        unique (businesstripID),
    constraint businesstrip_title_uindex
        unique (title)
);

create table employee
(
    employeeID    int auto_increment
        primary key,
    employee_name varchar(45) not null,
    title         varchar(45) not null,
    constraint Employee_employeeID_uindex
        unique (employeeID)
);

create table businesstrip_has_employee
(
    businesstrip_ID int not null,
    employee_ID     int not null,
    constraint businesstrip_has_employee_businesstrip_ID_employee_ID_uindex
        unique (businesstrip_ID, employee_ID),
    constraint businesstrip_has_employee_businesstrip_businesstripID_fk
        foreign key (businesstrip_ID) references businesstrip (businesstripID),
    constraint businesstrip_has_employee_employee_employeeID_fk
        foreign key (employee_ID) references employee (employeeID)
);

create table flight
(
    flightID      int auto_increment
        primary key,
    flight_number varchar(45) not null,
    employee_ID   int         not null,
    departure     varchar(45) not null,
    landing       varchar(45) not null,
    constraint flight_flightID_uindex
        unique (flightID),
    constraint flight_employee_employeeID_fk
        foreign key (employee_ID) references employee (employeeID)
);

create table meeting
(
    meetingID       int auto_increment
        primary key,
    description     varchar(45) not null,
    businesstrip_ID int         not null,
    title           varchar(45) not null,
    constraint meeting_meetingID_uindex
        unique (meetingID),
    constraint meeting_title_uindex
        unique (title),
    constraint meeting_businesstrip_businesstripID_fk
        foreign key (businesstrip_ID) references businesstrip (businesstripID)
);

