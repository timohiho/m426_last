<?php
include 'config.php';

// Create connection
try {
    global $DB_HOST, $DB_NAME, $DB_USER, $DB_PASSWORD;
    $pdo = new PDO("mysql:host=$DB_HOST;dbname=$DB_NAME", $DB_USER, $DB_PASSWORD);
    // set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

function db_getFlights() {
    global $pdo;
    $sql = "SELECT * FROM flight";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function db_getFlight($id) {
    global $pdo;
    $sql = "SELECT * FROM flight WHERE flightID = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id]);
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
}

function db_getEmployees() {
    global $pdo;
    $sql = "SELECT * FROM employee";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function db_getEmployee($id) {
    global $pdo;
    $sql = "SELECT * FROM employee WHERE employeeID = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id]);
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
}

function db_getBusinesstrip($id) {
    global $pdo;
    $sql = "SELECT * FROM businesstrip WHERE businesstripID = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id]);
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
}

function db_getBusinesstrips() {
    global $pdo;
    $sql = "SELECT * FROM businesstrip";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function db_getBusinesstripEmployees($id) {
    global $pdo;
    $sql = "SELECT * FROM businesstrip_has_employee WHERE businesstrip_ID = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id]);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function db_getMeetings() {
    global $pdo;
    $sql = "SELECT * FROM meeting";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function db_getMeeting($id) {
    global $pdo;
    $sql = "SELECT * FROM meeting WHERE meetingID = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id]);
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
}

function db_getMeetingsByBusinesstrip($id) {
    global $pdo;
    $sql = "SELECT * FROM meeting WHERE businesstrip_ID = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id]);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function db_addFlight($number, $departure_from, $destination, $employee_ID) {
    global $pdo;
    $sql = "INSERT INTO flight (flight_number, departure, landing, employee_ID) VALUES (:number, :departure_from, :destination, :employee_ID)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['number' => $number, 'departure_from' => $departure_from, 'destination' => $destination, 'employee_ID' => $employee_ID]);
}

function db_addEmployee($name, $title) {
    global $pdo;
    $sql = "INSERT INTO employee (employee_name, title) VALUES (:name, :title)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['name' => $name, 'title' => $title]);
}

function db_addBusinesstrip($description, $title) {
    global $pdo;
    $sql = "INSERT INTO businesstrip (description, title) VALUES (:description, :title)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['description' => $description, 'title' => $title]);
}

function db_addEmployeeToBusinesstrip($employee_ID, $businesstrip_ID) {
    global $pdo;
    $sql = "INSERT INTO businesstrip_has_employee (employee_ID, businesstrip_ID) VALUES (:employee_ID, :businesstrip_ID)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['employee_ID' => $employee_ID, 'businesstrip_ID' => $businesstrip_ID]);
}

function db_getBusinessTripMeetings($businesstripID)
{
    global $pdo;
    $sql = "SELECT * FROM meeting WHERE businesstrip_ID = :businesstripID";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['businesstripID' => $businesstripID]);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function db_addMeetingToBusinessTrip($businesstripID, $title, $description)
{
    global $pdo;
    $sql = "INSERT INTO meeting (title, description, businesstrip_ID) VALUES (:title, :description, :businesstripID)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['title' => $title, 'description' => $description, 'businesstripID' => $businesstripID]);
}
?>