<?php
require 'db.php';

// Get all flights
$flights = db_getFlights();


// Show Flights
echo '<h1>Flights</h1>';
echo '<table>';
echo '<tr>';
echo '<th>Flight ID</th>';
echo '<th>Flight Number</th>';
echo '<th>Flight Departure From</th>';
echo '<th>Flight Destination</th>';
echo '<th>Flight Employee</th>';
echo '</tr>';
foreach ($flights as $flight) {
    echo '<tr>';
    echo '<td>' . $flight['flightID'] . '</td>';
    echo '<td>' . $flight['flight_number'] . '</td>';
    echo '<td>' . $flight['departure'] . '</td>';
    echo '<td>' . $flight['landing'] . '</td>';
    echo '<td>' . db_getEmployee($flight['employee_ID'])['employee_name'] . '</td>';
    echo '</tr>';
}
echo '</table>';
?>

<a href="businesstrips.php">Business Trips</a><br>
<a href="addflight.php">Add Flight</a><br>
<a href="addemployee.php">Add Employee</a><br>